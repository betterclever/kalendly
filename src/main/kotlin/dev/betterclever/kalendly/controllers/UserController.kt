package dev.betterclever.kalendly.controllers

import SlotDuration
import dev.betterclever.kalendly.repositories.EventRepository
import dev.betterclever.kalendly.repositories.UserRepository
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import rangeTo
import java.sql.Connection
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.sql.DataSource

@RestController
@RequestMapping("/api/v1/users")
class UserController(
    val userRepository: UserRepository,
    val eventRepository: EventRepository,
    private val dataSource: DataSource
) {

    @PostMapping("/")
    fun createUser(@RequestBody user: AMCreateUser): AMUser {
        val userId = userRepository.create(user, dataSource.connection)
        return AMUser(
            id = userId,
            username = user.username
        )
    }

    @PatchMapping("/{id}/availability")
    fun addUserAvailability(
        @PathVariable("id") userId: Int,
        @RequestBody weekdayAvailabilities: ArrayList<AMWeekdayAvailability>
    ) {
        val availabilityByDayOfWeek = weekdayAvailabilities.associateBy { it.dayOfWeek }
        userRepository.updateAvailability(userId, availabilityByDayOfWeek, dataSource.connection)
    }

    @PostMapping("/{id}/schedule")
    fun scheduleEvent(
        @PathVariable("id") userId: Int,
        @RequestBody newEvent: AMCreateEvent,
    ) {
        val eventDate = newEvent.eventDate

        if (eventDate < LocalDate.now() || (eventDate == LocalDate.now() && newEvent.startTime.isBefore(LocalTime.now()))) {
            throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Event must be scheduled later than current time")
        }
        val conn = dataSource.connection
        val availabilityOfDay = userRepository.getAvailability(userId, conn)[eventDate.dayOfWeek]

        if (availabilityOfDay != null) {
            val eventTime = newEvent.startTime
            val eventEndTime = newEvent.startTime.plus(newEvent.duration.toDuration()).minusMinutes(1)

            if (newEvent.startTime.minute != 0 && newEvent.startTime.minute != 30) {
                throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Event must start at a round time")
            }

            if (eventTime <= availabilityOfDay.startTime && eventEndTime >= availabilityOfDay.endTime) {
                throw ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Event is requested outside of day's availability: ${availabilityOfDay.startTime} - ${availabilityOfDay.endTime}")
            }

            val currentEvents = eventRepository.getAllEventsForUser(userId, eventDate, eventDate, conn)
            currentEvents.forEach {
                val start1 = eventTime
                val start2 = it.startTime.toLocalTime()

                val end1 = eventEndTime
                val end2 = it.endTime.toLocalTime()

                val isOverlapping = start1.isBefore(end2) && end1.isAfter(start2)
                if (isOverlapping) {
                    throw ResponseStatusException(HttpStatus.CONFLICT, "Requested timeslot overlaps with existing event created on timeline of user: $userId")
                }
            }

            eventRepository.createEvent(
                AMEvent(
                    startTime = LocalDateTime.of(newEvent.eventDate, newEvent.startTime),
                    endTime = LocalDateTime.of(newEvent.eventDate, eventEndTime),
                    guestId = newEvent.guestId,
                    hostId = userId
                ), conn
            )
        }
    }

    private fun findAvailableSlotsForUser(
        userId: Int,
        startDate: LocalDate,
        endDate: LocalDate,
        slotDuration: SlotDuration,
        conn: Connection
    ): List<AMAvailableSlot> {
        val userAvailabilityByDayOfWeek = userRepository.getAvailability(userId, conn)

        // FIXME: On an assumption that an event will last a single day for now
        val currentScheduledEvents = eventRepository.getAllEventsForUser(userId, startDate, endDate, conn)
        val currentEventsByParticipatingDates = currentScheduledEvents.groupBy {
            it.startTime.toLocalDate()
        }

        val allSlots = startDate.rangeTo(endDate).flatMap { date ->
            val availability = userAvailabilityByDayOfWeek[date.dayOfWeek]
            if (availability != null) {
                val availableSlots = mutableListOf<AMAvailableSlot>()

                val startTime = availability.startTime
                val startTimeMinute = startTime.minute

                var slotStartTime = startTime
                slotStartTime = if (startTimeMinute in 1..29) {
                    LocalTime.of(startTime.hour, 30)
                } else {
                    LocalTime.of(startTime.hour + 1, 0)
                }

                val duration = when (slotDuration) {
                    SlotDuration.MIN30 -> Duration.ofMinutes(30)
                    SlotDuration.MIN60 -> Duration.ofMinutes(60)
                }
                var curTime = slotStartTime

                while (curTime + duration.minusMinutes(1) < availability.endTime) {
                    availableSlots.add(
                        AMAvailableSlot(
                            date = date,
                            startTime = curTime,
                            endTime = curTime + duration.minusMinutes(1)
                        )
                    )
                    curTime += duration
                }

                currentEventsByParticipatingDates[date]?.forEach { event ->
                    availableSlots.removeIf {
                        val start1 = LocalDateTime.of(it.date, it.startTime)
                        val end1 = LocalDateTime.of(it.date, it.endTime)

                        val start2 = event.startTime
                        val end2 = event.endTime

                        !start1.isAfter(end2) && !start2.isAfter(end1)
                    }
                }

                availableSlots
            } else {
                emptyList()
            }
        }

        return allSlots
    }

    @GetMapping("/{id}/available-slots")
    fun getAvailableSlots(
        @PathVariable("id") userId: Int,
        @RequestParam("start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) startDate: LocalDate,
        @RequestParam("end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) endDate: LocalDate,
        @RequestParam("slot_duration") slotDuration: SlotDuration,
        /// Guest id is optional, if it is passed, the slots are returned with overlapping availabilities of both people
        @RequestParam("guest_id") guestId: Int?
    ): List<AMAvailableSlot> {

        val conn = dataSource.connection
        val hostAvailableSlots = findAvailableSlotsForUser(userId, startDate, endDate, slotDuration, conn)
        return if (guestId != null) {
            val guestAvailableSlots = findAvailableSlotsForUser(guestId, startDate, endDate, slotDuration, conn)
            hostAvailableSlots.intersect(guestAvailableSlots.toSet()).toList()
        } else {
            hostAvailableSlots
        }
    }

    @GetMapping("/{id}/events")
    fun getEvents(
        @PathVariable("id") userId: Int,
        @RequestParam("start_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) startDate: LocalDate,
        @RequestParam("end_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) endDate: LocalDate,
    ): List<AMEvent> {

        val conn = dataSource.connection
        val userEvents = eventRepository.getAllEventsForUser(userId, startDate, endDate, conn)

        return userEvents.map {
            AMEvent(
                hostId = it.hostId,
                guestId = it.guestId,
                startTime = it.startTime,
                endTime = it.endTime
            )
        }
    }
}