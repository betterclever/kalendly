package dev.betterclever.kalendly.controllers

import dev.betterclever.kalendly.repositories.EventRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalDateTime
import javax.sql.DataSource

@RestController
@RequestMapping("/api/v1/events")
class EventController(
    val eventRepository: EventRepository,
    val dataSource: DataSource
) {

    @GetMapping
    fun getEvents(
        @RequestParam("guest_id") guestId: Int,
        @RequestParam("host_id") hostId: Int,
        @RequestParam("start_date") startDate: LocalDate,
        @RequestParam("end_date") endDate: LocalDate
    ): List<AMEvent> {
        val conn = dataSource.connection
        val events = eventRepository.getEvents(
            guestId = guestId,
            hostId = hostId,
            startDate = startDate,
            endDate = endDate,
            conn = conn
        )

        return events.map { AMEvent.from(it) }
    }
}