package dev.betterclever.kalendly.controllers

import SlotDuration
import dev.betterclever.kalendly.db.tables.records.EventRecord
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

data class AMCreateUser(
    val username: String,
)

data class AMUser(
    val id: Int,
    val username: String
)

data class AMWeekdayAvailability(
    val dayOfWeek: DayOfWeek,
    val startTime: LocalTime,
    val endTime: LocalTime,
)

data class AMCreateEvent(
    val eventDate: LocalDate,
    val startTime: LocalTime,
    val duration: SlotDuration,
    val guestId: Int
)

data class AMSlotFindRequest(
    val guestId: Int,
    val startDate: LocalDate,
    val endDate: LocalDate,
    val duration: SlotDuration
)

data class AMEvent(
    val startTime: LocalDateTime,
    val endTime: LocalDateTime,
    val hostId: Int,
    val guestId: Int
) {
    companion object {
        fun from(eventRecord: EventRecord): AMEvent = AMEvent(
            startTime = eventRecord.startTime,
            endTime = eventRecord.endTime,
            guestId = eventRecord.guestId,
            hostId = eventRecord.hostId
        )
    }
}

data class AMAvailableSlot(
    val date: LocalDate,
    val startTime: LocalTime,
    val endTime: LocalTime
)