package dev.betterclever.kalendly.repositories

import dev.betterclever.kalendly.controllers.AMEvent
import dev.betterclever.kalendly.db.Tables
import dev.betterclever.kalendly.db.tables.records.EventRecord
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository
import java.sql.Connection
import java.time.LocalDate

@Repository
class EventRepository {

    fun createEvent(amEvent: AMEvent, conn: Connection): Int {
        val context = DSL.using(conn, SQLDialect.POSTGRES)
        val id = context.insertInto(Tables.EVENT)
            .columns(Tables.EVENT.START_TIME, Tables.EVENT.END_TIME, Tables.EVENT.HOST_ID, Tables.EVENT.GUEST_ID)
            .values(amEvent.startTime, amEvent.endTime, amEvent.hostId, amEvent.guestId)
            .returningResult(Tables.EVENT.ID)
            .fetch()

        return id[0].value1()
    }

    fun getAllEventsForUser(userId: Int,
                            startDate: LocalDate,
                            endDate: LocalDate,
                            conn: Connection): List<EventRecord> {
        val context = DSL.using(conn, SQLDialect.POSTGRES)
        return context.selectFrom(Tables.EVENT)
            .where(Tables.EVENT.GUEST_ID.eq(userId)
                .or(Tables.EVENT.HOST_ID.eq(userId))
                .and(Tables.EVENT.START_TIME.ge(startDate.atStartOfDay()))
                .and(Tables.EVENT.END_TIME.lessThan(endDate.plusDays(1).atStartOfDay()))
            )
            .fetch()
    }

    fun getEvents(hostId: Int, guestId: Int, startDate: LocalDate, endDate: LocalDate, conn: Connection): List<EventRecord> {
        val context = DSL.using(conn, SQLDialect.POSTGRES)
        return context.selectFrom(Tables.EVENT)
            .where(Tables.EVENT.HOST_ID.eq(hostId))
            .and(Tables.EVENT.GUEST_ID.eq(guestId))
            .and(Tables.EVENT.START_TIME.ge(startDate.atStartOfDay()))
            .and(Tables.EVENT.END_TIME.lessThan(endDate.plusDays(1).atStartOfDay()))
            .fetch()
    }
}