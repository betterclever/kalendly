create table users(
    id serial primary key,
    username varchar(100) unique not null
);

create type day_of_week as enum (
    'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'
);

create table weekday_availability(
    id serial primary key,
    user_id int not null,
    day_of_week day_of_week not null,
    start_time time not null,
    end_time time not null,
    constraint fk_user_availability foreign key(user_id) references users(id),
    constraint uk_user_id_dow unique (user_id, day_of_week),
    constraint check_start_time_less_than_end_time check ( start_time < end_time )
);

create table event(
    id serial primary key,
    host_id int not null,
    guest_id int not null,
    start_time timestamp not null,
    end_time timestamp not null,
    constraint fk_event_host_user foreign key(host_id) references users(id),
    constraint fk_event_guest_user foreign key (guest_id) references users(id),
    constraint check_event_start_time_less_than_end_time check ( start_time < event.end_time )
);